const path = require('path');

module.exports = {
    webpack: {
        alias: {
            "@apis": path.resolve(__dirname, "src/shared/api"),
            "@utils": path.resolve(__dirname, "src/shared/utils"),
            "@components": path.resolve(__dirname, "src/shared/components"),
            "@constants": path.resolve(__dirname, "src/shared/constants"),
            "@hooks": path.resolve(__dirname, "src/shared/hooks"),
            "@custom-types": path.resolve(__dirname, "src/shared/types"),
            "@shared": path.resolve(__dirname, "src/shared"),
            "@app": path.resolve(__dirname, "src"),
            "@features": path.resolve(__dirname, "src/features"),
            "@apis/": path.resolve(__dirname, "src/shared/api/"),
            "@utils/": path.resolve(__dirname, "src/shared/utils/"),
            "@components/": path.resolve(__dirname, "src/shared/components/"),
            "@constants/": path.resolve(__dirname, "src/shared/constants/"),
            "@hooks/": path.resolve(__dirname, "src/shared/hooks/"),
            "@custom-types/": path.resolve(__dirname, "src/shared/types/"),
            "@shared/": path.resolve(__dirname, "src/shared/"),
            "@app/": path.resolve(__dirname, "src/"),
            "@features/": path.resolve(__dirname, "src/features/"),
            "@assets": path.resolve(__dirname, "src/assets/")
        },
    },
};