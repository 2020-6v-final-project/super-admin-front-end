import { Action, combineReducers, configureStore } from "@reduxjs/toolkit";
import { ThunkAction } from "redux-thunk";
import { authReducer } from "./state/authenticate/authReducer";
import { userReducer } from "./state/user/userReducer";
import ContractManagementSlice from "@features/HomePage/ContractManagement/ContractManagement.slice";
import CompanyManagementSlice from "./features/HomePage/CompanyManagement/CompanyManagement.slice";

const rootReducer = combineReducers({
  user: userReducer,
  auth: authReducer,
  company: CompanyManagementSlice,
  contract: ContractManagementSlice
});

const store = configureStore({ reducer: rootReducer });

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
export type AppDispatch = typeof store.dispatch;

export default store;
