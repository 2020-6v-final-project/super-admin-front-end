export type Contract = {
  id: number;
  companyid: number;
  start: Date;
  end: Date;
  statusid: number;
  statuscode: string;
  typeid: number;
};
