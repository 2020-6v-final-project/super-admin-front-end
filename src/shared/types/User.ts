export interface User {
  username: String;
  firstname: String;
  middlename: String;
  lastname: String;
  phone: String;
  dateofbirth: String | Date;
  address: String;
  ismale: Boolean | String;
  email: String;
  joinedat: "" | String | Date;
  companyid: Number;
  companycode: String;
}
