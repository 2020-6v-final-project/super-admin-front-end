export type ContractError = {
  [key: string]: string | boolean;
  id: string | boolean;
  companyid: string | boolean;
  start: string | boolean;
  end: string | boolean;
  statusid: string | boolean;
  statuscode: string | boolean;
  typeid: string | boolean;
};
