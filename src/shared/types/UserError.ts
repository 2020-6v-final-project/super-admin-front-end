export interface UserError {
  username: string | boolean;
  firstname: string | boolean;
  middlename: string | boolean;
  lastname: string | boolean;
  phone: string | boolean;
  dateofbirth: string | boolean;
  address: string | boolean;
  email: string | boolean;
  [key: string]: string | boolean;
}
