export interface Company {
  id: number;
  code: string;
  name: string;
  owner: number | null;
  statusid: number;
  statuscode: string;
  numberofusers: number;
  numberofbranches: number;
  businesscontract: number;
}
