export interface CompanyError {
  code: string | boolean;
  name: string | boolean;
  owner: string | boolean;
  [key: string]: string | boolean;
}
