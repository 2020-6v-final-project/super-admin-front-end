export type Status = {
  id: number;
  code: string;
  name: string;
};
