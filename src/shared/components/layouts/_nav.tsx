
const navigation = [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Thống kê'],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Tổng quan',
    to: '/dashboard',
    icon: "cil-speedometer",
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Quản lý'],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Hợp đồng',
    to: '/contract-management',
    icon: "cil-dollar",
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Công ty',
    to: '/company-management',
    icon: "cil-library-building",
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Người dùng',
    to: '/user-management',
    icon: "cil-user-plus",
  }
]


export default navigation;