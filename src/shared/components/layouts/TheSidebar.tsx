import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from "@coreui/react";
import logo from "@assets/logo_trans.png";
// sidebar nav config
import navigation from "./_nav";
import logo_minimized from '@assets/login_img.png';

const TheSidebar: React.FC = () => {
  const dispatch = useDispatch();
  const show = useSelector((state: any) => state.sidebarShow);

  return (
    <CSidebar
      show={show}
      colorScheme="light"
      onShowChange={(val: any) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand
        className="d-md-down-none py-3"
        style={{ backgroundColor: "white" }}
        to="/"
      >
        <img
          className="c-sidebar-brand-full"
          alt="logo"
          src={logo}
          style={{ height: 35 }}
        />
        <img
          className="c-sidebar-brand-minimized"
          alt="logo"
          src={logo_minimized}
          style={{ height: 35 }}
        />
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
