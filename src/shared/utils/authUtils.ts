export function authUtils() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user')as any);

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}