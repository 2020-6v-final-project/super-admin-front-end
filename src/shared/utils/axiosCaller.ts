import axios from "axios";
import config from "../api/config.json";
export const axiosCaller = function (
  params: any,
  targetUrl: any,
  method: any,
  callback: any,
  responseType: any,
  data: any,
  contentType: any
) {
  if (targetUrl.startsWith("/")) {
    targetUrl = config["apiStagging-baseURL"] + targetUrl;
  }
  if (!responseType) {
    responseType = "json";
  }
  if (!data) {
    data = "";
  }
  if (!contentType) {
    contentType = "application/json";
  }
  return axios
    .request({
      url: targetUrl,
      method,
      responseType,
      params,
      data: JSON.stringify({ ...data }),
      headers: {
        "Content-Type": contentType,
        Authorization: "bearer " + localStorage.getItem("user"),
      },
    })
    .then((response) => {
      if (typeof callback === "function") {
        return callback(response.data);
      } else {
        return response;
      }
    });
};
