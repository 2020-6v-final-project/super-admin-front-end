import * as yup from "yup";

export const UserSchema = yup.object().shape({
  email: yup.string().email(),
  username: yup.string().required().min(5).max(20),
  firstname: yup.string().required().min(2).max(20),
  middlename: yup.string().min(0).max(20),
  lastname: yup.string().required().min(2).max(20),
  phone: yup.string().required().min(10).max(12),
  dateofbirth: yup.string().required(),
  address: yup.string().required().min(5).max(100),
  ismale: yup.string().required()
});

export const CompanySchema = yup.object().shape({
  code: yup.string().required().min(1).max(20),
  name: yup.string().required().min(1).max(20),
  owner: yup.number().nullable().min(0).default(0)
});

export const ContractSchema = yup.object().shape({
  id: yup.number().required(),
  companyid: yup.number().required(),
  start: yup.date().required().max(new Date()),
  end: yup.date().required().min(new Date()),
  statusid: yup.number().required(),
  statuscode: yup.string().required(),
  typeid: yup.number().required()
});
