import {useState} from 'react';

type ToggleProps = [boolean, Function];

function useToggle (initialState: boolean): ToggleProps{
  const [value, setValue] = useState(initialState);
  const toggle = () => { setValue(!value) };
  
  return [value, toggle];
};

export default useToggle;