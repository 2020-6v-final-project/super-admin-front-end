// import {authUtils} from '../utils/authUtils'
import config from "./config.json";
import { axiosCaller } from "../utils/axiosCaller";
import { history } from "../components/history";
import { User } from "../types/User";

export const userAPI = {
  login,
  logout,
  getUserList
};

function login(username: string, password: string) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ username, password })
  };
  return fetch(`${config["apiStagging-login"]}`, requestOptions)
    .then(handleResponse)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("user", user.token);
      history.push("/dashboard");
      return user;
    });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem("user");
}

function handleResponse(response: any) {
  return response.text().then((text: any) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

function getUserList(callback: Function) {
  axiosCaller(
    {},
    "/admin/users",
    "GET",
    (response: any) => {
      callback(response);
    },
    "",
    "",
    ""
  );
}

export function addUser(user: User, callback: Function) {
  axiosCaller(
    {},
    "/admin/users/",
    "POST",
    (response: any) => {
      callback(response);
    },
    "",
    user,
    ""
  );
}

export function updateUser(user: any, callback: Function) {
  console.log(user);
  axiosCaller(
    {},
    "/admin/users/" + user.id,
    "PUT",
    (response: any) => {
      callback(response);
    },
    "",
    user,
    ""
  );
}
