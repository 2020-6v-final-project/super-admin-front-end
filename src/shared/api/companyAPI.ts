import { axiosCaller } from "../utils/axiosCaller";
import { Company } from "../types/Company";

export function getCompanyList(callback: Function) {
  axiosCaller(
    {},
    "/admin/companies/",
    "GET",
    (response: any) => {
      callback(response);
    },
    "",
    "",
    ""
  );
}

export function addCompany(company: Company, callback: Function) {
  axiosCaller(
    {},
    "/admin/companies/",
    "POST",
    (response: any) => {
      callback(response);
    },
    "",
    company,
    ""
  );
}

export function updateCompany(company: Company, callback: Function) {
  axiosCaller(
    {},
    "/admin/companies/" + company.id,
    "PUT",
    (response: any) => {
      callback(response);
    },
    "",
    company,
    ""
  );
}

export function deactivateCompany(companyId: number, callback: Function) {
  axiosCaller(
    {},
    "/admin/companies/deactivate/" + companyId,
    "POST",
    (response: any) => {
      callback(response);
    },
    "",
    companyId,
    ""
  );
}
