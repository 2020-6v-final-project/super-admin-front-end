import { axiosCaller } from "../utils/axiosCaller";
import { Contract } from "../types/Contract";

export function getContractList(callback: Function) {
  axiosCaller(
    {},
    "/admin/contracts/",
    "GET",
    (response: any) => {
      callback(response);
    },
    "",
    "",
    ""
  );
}

export function addContract(contract: Contract, callback: Function) {
  axiosCaller(
    {},
    "/admin/contracts/",
    "POST",
    (response: any) => {
      callback(response);
    },
    "",
    contract,
    ""
  );
}

export function updateContract(contract: Contract, callback: Function) {
  axiosCaller(
    {},
    "/admin/contracts/" + contract.id,
    "PUT",
    (response: any) => {
      callback(response);
    },
    "",
    contract,
    ""
  );
}

export function deactivateContract(contractId: number, callback: Function) {
  axiosCaller(
    {},
    "/admin/contracts/deactivate/" + contractId,
    "POST",
    (response: any) => {
      callback(response);
    },
    "",
    contractId,
    ""
  );
}
