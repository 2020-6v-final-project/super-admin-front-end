import { CContainer } from '@coreui/react';
import React, { Suspense } from 'react';
import {
    Switch
} from 'react-router-dom';
import { TheHeader, TheSidebar } from '@components';
import './HomePage.scss';

const loading = (
    <div className="pt-3 text-center">
        <div className="sk-spinner sk-spinner-pulse"></div>
    </div>
)

const HomePageContainer: React.FC = () => {
    return (
        <div className="c-app c-default-layout">
            <TheSidebar />
            <div className="c-wrapper">
                <TheHeader />
                <div className="c-body">
                    <main className="c-main">
                        <CContainer fluid>
                            <Suspense fallback={loading}>
                                <Switch>

                                </Switch>
                            </Suspense>
                        </CContainer>
                    </main>
                </div>
            </div>
        </div>
    );
}

export default HomePageContainer;