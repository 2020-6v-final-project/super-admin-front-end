import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CDataTable,
  CInputGroup,
  CRow
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { userActions } from "../../../../../state/user/userActions";
import AddUserModal from "../add-user-modal/AddUserModal";
import UserDetailModal from "../user-detail-modal/UserDetailModal";
import "./ListUsers.scss";
const ListUsers = (props: any) => {
  const [listCompanies, setListCompanies] = useState<any>([])
  //State for opening and closing modal
  const [openAddUserModal, setOpenAddUserModal] = useState<boolean>(false);
  const [openUserDetailModal, setOpenUserDetailModal] = useState<boolean>(
    false
  );

  const [userDetail, setUserDetail] = useState<any>(null);

  useEffect(() => {
  }, []);

  const onAddUserToggle: any = () => setOpenAddUserModal(!openAddUserModal);

  const onUserDetailToggle: any = (item: any) => {
    setUserDetail(item);
    setOpenUserDetailModal(!openUserDetailModal);
  };

  return (
    <CRow>
      <CCol>
        <AddUserModal isShow={openAddUserModal} onFinish={onAddUserToggle} listCompanies={listCompanies} />
        <UserDetailModal
          isShow={openUserDetailModal}
          onClose={onUserDetailToggle}
          data={userDetail}
        />
        <CCard>
          <CCardBody>
            <CInputGroup className="justify-content-between px-0 mx-0">
              <CRow className="col-md-12 px-0 my-2">
                <CCol sm="10">
                  <h4>Quản lý tài khoản</h4>
                </CCol>
                <CCol className="px-0" sm="2">
                  <CButton
                    color="warning"
                    className="float-right"
                    style={{ color: "white" }}
                    onClick={onAddUserToggle}
                  >
                    {" "}
                    <CIcon className="mr-2" content={freeSet.cilUserPlus} />
                    Tạo tài khoản
                  </CButton>
                </CCol>
              </CRow>
            </CInputGroup>

            <CDataTable
              fields={[
                { key: "#", _classes: "font-weight-bold" },
                "Tên tài khoản",
                "Họ và tên",
                "Email",
                "Công ty",
                "Trạng thái",
              ]}
              hover
              sorter
              tableFilter
              itemsPerPageSelect
              border={false}
              outlined={false}
              striped
              itemsPerPage={10}
              footer={false}
              activePage={0}
              pagination
              clickableRows
              items={props.userList.map((item: any, index: any) => {
                return {
                  "#": index + 1,
                  id: item.id,
                  firstname: "" || item.firstname,
                  middlename: "" || item.middlename,
                  lastname: "" || item.lastname,
                  dateofbirth: "" || item.dateofbirth,
                  address: "" || item.address,
                  ismale: true || item.ismale,
                  joinedat: "" || item.joinedat,
                  validatetoken: null || item.validatetoken,
                  validatetokenexpiredat: null || item.validatetokenexpiredat,
                  phone: '' || item.phone,
                  "Tên tài khoản": item.email
                    ? item.email.slice(0, item.email.indexOf("@"))
                    : "",
                  "Họ và tên":
                    "" ||
                    item.lastname +
                    " " +
                    item.middlename +
                    " " +
                    item.firstname,
                  Email: "" || item.email,
                  "Công ty": "" || item.companycode,
                  companyid: "" || item.companyid,
                  statusid: "" || item.statusid,
                  statusCode: "" || item.statuscode,
                };
              })}
              onRowClick={(item: any) => onUserDetailToggle(item)}
              scopedSlots={{
                "Trạng thái": (item: any) => (
                  <td>
                    <CBadge color="success">{item.statusCode}</CBadge>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

function mapStateToProps(state: any) {
  return {
    userList: state.user.userList
  };
}

const mapDispatchToProps = { setUserList: userActions.setUserList };

export default connect(mapStateToProps, mapDispatchToProps)(ListUsers);
