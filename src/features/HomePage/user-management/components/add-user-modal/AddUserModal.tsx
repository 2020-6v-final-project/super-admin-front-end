import {
  CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,

  CInputGroup,
  CModal, CSelect
} from "@coreui/react";
import React, { ChangeEvent, FC, FormEvent, useState } from "react";
import { connect } from "react-redux";
import swal from 'sweetalert';
import userPlusImg from "../../../../../assets/user-plus.png";
import { addUser } from "../../../../../shared/api/userAPI";
import { User } from '../../../../../shared/types/User';
import { UserError } from '../../../../../shared/types/UserError';
import { UserSchema } from "../../../../../shared/utils/validation";
import { setUserList } from "../../../../../state/user/userActions";
import "./AddUserModal.scss";

const AddUserModal: FC<any> = (props) => {

  const [formData, setFormData] = useState({
    username: "",
    firstname: "",
    middlename: "",
    lastname: "",
    phone: "",
    dateofbirth: "",
    address: "",
    ismale: "true",
    email: "",
    joinedat: "",
    companyid: 0,
    companycode: ""
  });
  const [userCompany, setUserCompany] = useState({}) as any;
  const [formDataError, setFormDataError] = useState<UserError>({
    username: false,
    firstname: false,
    middlename: false,
    lastname: false,
    phone: false,
    dateofbirth: false,
    address: false,
    email: false,
  });

  const onChangeCompany = (e: ChangeEvent<HTMLInputElement>) => {
    for (let a = 0; a < props.listCompanies.length; a++)
    {
      if (props.listCompanies[a].id === e.target.value)
      {
        setUserCompany({ "id": props.listCompanies[a].id, "code": props.listCompanies[a].code })
      }
    }
  }

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(props);
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const user = formData as User;
    user.companyid = userCompany.id;
    user.companycode = userCompany.code;
    //Set joinedat
    user.joinedat = new Date().toISOString().slice(0, 10);
    console.log(user);
    //Validaten form data
    UserSchema.validate(formData, { abortEarly: false }).then(
      async (valid: Object) => {
        //Call server
        await addUser(user, (data: any) => {
          swal("Thêm thành công!", "Người dùng đã được thêm vào hệ thống!", "success");
          props.onFinish();
        });
      },
      (errors: any) => {
        let newFormDataError: UserError = { ...formDataError };

        for (let error of errors.errors)
        {
          let index: string = error.substring(0, error.indexOf(" "));
          newFormDataError[index] = true;
        }
        setFormDataError(newFormDataError);
        let keyDataError = "";
        for (const [key, value] of Object.entries(newFormDataError))
        {
          if (value === true) 
          {
            if (keyDataError !== "") keyDataError += ", ";
            keyDataError += key;
          }
        }
        if (keyDataError !== "")
          swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
      }
    );
  };
  return (
    <CModal
      centered
      backdrop
      closeOnBackdrop
      show={props.isShow}
      className="AddUserModal-styles"
    >
      <CCard className="align-middle justify-content-center jus AddUserModal-card-styles">
        <CCardBody className="AddUserModal-cardBody-styles">
          <h4 className="AddUserModal-h2-styles">Tạo tài khoản</h4>
          <img
            alt=""
            src={userPlusImg}
            className="AddUserModal-img-styles mx-auto my-3"
          />
          <CForm onSubmit={handleSubmit}>
            <CInputGroup>
              {/* <CInput
                onChange={onChange}
                placeholder="Tên công ty"
                className="AddUserModal-input-styles"
                type="text"
                name="company"
              /> */}
              <CSelect
                type="select" className="AddUserModal-input-styles form-control" onChange={onChangeCompany}>
                <option hidden>Tên công ty</option>
                {props.listCompanies.map((item: any) =>
                  <option key={item.id} value={item.id}>{item.name}</option>
                )}
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.username ? true : false}
                onChange={onChange}
                required
                placeholder="Tên tài khoản"
                className="AddUserModal-input-styles"
                type="text"
                name="username"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.email ? true : false}
                onChange={onChange}
                required
                placeholder="Email"
                className="AddUserModal-input-styles"
                type="text"
                name="email"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.lastname ? true : false}
                onChange={onChange}
                required
                placeholder="Họ"
                className="AddUserModal-input-styles"
                type="text"
                name="lastname"
              />
              <CInput
                invalid={formDataError.middlename ? true : false}
                onChange={onChange}
                placeholder="Tên đệm"
                className="AddUserModal-input-styles"
                type="text"
                name="middlename"
              />
              <CInput
                invalid={formDataError.firstname ? true : false}
                onChange={onChange}
                required
                placeholder="Tên"
                className="AddUserModal-input-styles"
                type="text"
                name="firstname"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.dateofbirth ? true : false}
                onChange={onChange}
                required
                placeholder="Ngày sinh"
                className="AddUserModal-input-styles"
                type="date"
                name="dateofbirth"
              />
              <CSelect
                onChange={onChange}
                required
                defaultValue="true"
                placeholder="Giới tính"
                className="AddUserModal-input-styles"
                type="select"
                name="ismale"
              >
                <option value="true">Nam</option>
                <option value="false">Nữ</option>
              </CSelect>
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.phone ? true : false}
                onChange={onChange}
                required
                placeholder="Số điện thoại"
                className="AddUserModal-input-styles"
                type="number"
                name="phone"
              />
              <CInput
                invalid={formDataError.address ? true : false}
                onChange={onChange}
                required
                placeholder="Địa chỉ"
                className="AddUserModal-input-styles"
                type="text"
                name="address"
              />
            </CInputGroup>
            <CButton
              required
              type="submit"
              color="primary"
              className="AddUserModal-submitButton-styles btn my-3"
            >
              Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

const mapStateToProps = (state: any) => {
  return {
    userList: state.user.userList,
  };
};

const mapDispatchToProps = { setUserList };

export default connect(mapStateToProps, mapDispatchToProps)(AddUserModal);
