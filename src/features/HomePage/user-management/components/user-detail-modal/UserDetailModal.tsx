import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,
  CInputGroup,
  CModal
} from '@coreui/react';
import React, { ChangeEvent, FC, FormEvent, useEffect, useState } from 'react';
import { connect } from "react-redux";
import swal from 'sweetalert';
import userPlusImg from '../../../../../assets/user-plus.png';
import { updateUser } from "../../../../../shared/api/userAPI";
import { UserError } from '../../../../../shared/types/UserError';
import { UserSchema } from "../../../../../shared/utils/validation";
import { setUserList } from "../../../../../state/user/userActions";
import './UserDetailModal.scss';

// { isShow, onClose, data ,props}
const UserDetailModal: FC<any> = (props) => {
  const [isEditMode, setEditMode] = useState(false);
  const toggleEditMode = () => {
    setEditMode(!isEditMode);
  }
  const [formData, setFormData] = useState({
    id: "",
    username: "",
    firstname: "",
    middlename: "",
    lastname: "",
    email: "",
    dateofbirth: "",
    address: "",
    phone: "",
    ismale: "",
    joinedat: "",
    validatetoken: "",
    validatetokenexpiredat: "",
    statusid: "",
    statuscode: "",
    companyid: "",
    companycode: ""
  });

  const [formDataError, setFormDataError] = useState<UserError>({
    username: false,
    firstname: false,
    middlename: false,
    lastname: false,
    phone: false,
    dateofbirth: false,
    address: false,
    email: false,
  });

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };
  useEffect(() => {
    if (props.data)
      setFormData({
        id: props.data.id,
        username: props.data["Tên tài khoản"],
        firstname: props.data.firstname,
        middlename: props.data.middlename,
        lastname: props.data.lastname,
        email: props.data.Email,
        dateofbirth: props.data.dateofbirth,
        address: props.data.address,
        phone: props.data.phone,
        ismale: props.data.ismale,
        joinedat: props.data.joinedat,
        validatetoken: props.data.validatetoken,
        validatetokenexpiredat: props.data.validatetokenexpiredat,
        statusid: props.data.statusid,
        statuscode: props.data.statuscode,
        companyid: props.data.companyid,
        companycode: props.data["Công ty"]
      });
  }, [props.data]);
  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const user = formData as any;
    user.id = props.data.id;
    user.ismale = props.data.ismale;
    user.joinedat = props.data.joinedat;
    user.validatetoken = props.data.validatetoken;
    user.validatetokenexpiredat = props.data.validatetokenexpiredat;
    user.statusid = props.data.statusid;
    user.statuscode = props.data.statusCode;
    user.companyid = props.data.companyid;

    //Validaten form data
    UserSchema.validate(formData, { abortEarly: false }).then(
      async (valid: Object) => {
        //Call server
        await updateUser(user, (data: any) => {
          swal("Thay đổi thành công!", "Thông tin đã được thay đổi!", "success");
          props.onClose();
        });
      },
      (errors: any) => {
        let newFormDataError: UserError = { ...formDataError };

        for (let error of errors.errors)
        {
          let index: string = error.substring(0, error.indexOf(" "));
          newFormDataError[index] = true;
        }
        setFormDataError(newFormDataError);
        let keyDataError = "";
        for (const [key, value] of Object.entries(newFormDataError))
        {
          if (value === true) 
          {
            if (keyDataError !== "") keyDataError += ", ";
            keyDataError += key;
          }
        }
        if (keyDataError !== "")
          swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
      }
    );
  };
  return (
    <CModal
      centered
      backdrop
      show={props.isShow}
      onClose={props.onClose}
      className="UserDetailModal-styles">
      <CCard className="align-middle UserDetailModal-card-styles" >
        <CCardBody className="UserDetailModal-cardBody-styles">
          <h4 className="UserDetailModal-h2-styles" >Thông tin tài khoản</h4>
          <img alt="" src={userPlusImg} className="UserDetailModal-img-styles my-3" />
          <p className="UserDetailModal-p-styles" >{props.data ? <CBadge color="success">{props.data.statusCode}</CBadge> : ''}</p>
          <CButton size="small" className="UserDetailModal-optionButton-styles btn-sm btn-outline-warning" onClick={toggleEditMode}>{isEditMode ? "Huỷ" : "Sửa"}</CButton>
          <CButton size="small" className="UserDetailModal-optionButton-styles btn-sm btn-outline-danger" >Chặn</CButton>
          <CForm onSubmit={handleSubmit}>
            <CInputGroup >
              <CInput onChange={onChange}
                readOnly={true}
                placeholder="Tên công ty"
                className="UserDetailModal-input-styles"
                type="text" name="companycode"
                defaultValue={props.data ? props.data["Công ty"] : ''}
              />
            </CInputGroup>
            <CInputGroup >
              <CInput onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Tên tài khoản"
                className="UserDetailModal-input-styles"
                type="text" name="username"
                defaultValue={props.data ? props.data['Tên tài khoản'] : ''} />
            </CInputGroup>
            <CInputGroup >
              <CInput onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Email"
                className="UserDetailModal-input-styles"
                type="text" name="email"
                defaultValue={props.data ? props.data['Email'] : ''} />
            </CInputGroup>
            <CInputGroup >

              <CInput readOnly={!isEditMode}
                invalid={formDataError.lastname ? true : false}
                onChange={onChange}
                required
                placeholder="Họ"
                className="AddUserModal-input-styles"
                defaultValue={props.data ? props.data.lastname : ''}
                type="text"
                name="lastname"
              />
              <CInput readOnly={!isEditMode}
                invalid={formDataError.middlename ? true : false}
                onChange={onChange}
                placeholder="Tên đệm"
                className="AddUserModal-input-styles"
                defaultValue={props.data ? props.data.middlename : ''}
                type="text"
                name="middlename"
              />
              <CInput readOnly={!isEditMode}
                invalid={formDataError.firstname ? true : false}
                onChange={onChange}
                required
                placeholder="Tên"
                className="AddUserModal-input-styles"
                defaultValue={props.data ? props.data.firstname : ''}
                type="text"
                name="firstname"
              />
            </CInputGroup>
            <CInputGroup >
              <CInput onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Số điện thoại"
                className="UserDetailModal-input-styles"
                type="number"
                name="phone"
                defaultValue={props.data ? props.data.phone : ''}
              />
              <CInput onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Ngày sinh"
                className="UserDetailModal-input-styles"
                type="date"
                name="dateofbirth"
                defaultValue={props.data && props.data.dateofbirth ? props.data.dateofbirth.substring(0, 10) : ''}
              />
            </CInputGroup>
            <CInputGroup >
              <CInput onChange={onChange}
                readOnly={!isEditMode}
                placeholder="Địa chỉ"
                className="UserDetailModal-input-styles"
                type="text" name="address"
                defaultValue={props.data ? props.data.address : ''} />
            </CInputGroup>
            <CButton color="primary" type='submit' className="UserDetailModal-submitButton-styles my-3">Lưu</CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
}
const mapStateToProps = (state: any) => {
  return {
    userList: state.user.userList,
  };
};

const mapDispatchToProps = { setUserList };

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailModal);