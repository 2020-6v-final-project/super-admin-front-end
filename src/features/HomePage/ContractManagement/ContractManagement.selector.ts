import { RootState } from "src/store";
import { createSelector } from "reselect";

export const contractListSelector = createSelector(
  [(state: RootState) => state.contract.contractList],
  (contractList) => {
    return contractList;
  }
);
