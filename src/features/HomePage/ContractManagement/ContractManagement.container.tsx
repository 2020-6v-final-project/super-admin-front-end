import { CFade } from "@coreui/react";
import React from "react";
import ContractStatistics from './components/ContractStatistics/ContractStatistics';
import ListContracts from "./components/ListContracts/ListContracts";
import { contractListSelector } from './ContractManagement.selector';
import { connect } from 'react-redux';
import { Contract } from '@custom-types';
import { fetchContractListAsync, addContractAsync, deactivateContractAsync, updateContractAsync } from './ContractManagement.slice';
import { RootState } from 'src/store';

type ContractManagementState = {
    contractList: Contract[],
}

type ContractManagementDispatch = {
    fetchContractListAsync: Function,
    addContractAsync: Function,
    deactivateContractAsync: Function,
    updateContractAsync: Function,
}

export type ContractManagementDefaultProps = ContractManagementState & ContractManagementDispatch;

const ContractManagementContainer: React.FC<ContractManagementDefaultProps> = React.memo((props) => {
    return (
        <CFade>
            <ContractStatistics />
            <ListContracts {...props} />
        </CFade>
    );
});

const mapState = (state: RootState) => {
    return {
        ContractList: contractListSelector(state),
    }
}
const mapDispatch = { fetchContractListAsync, addContractAsync, deactivateContractAsync, updateContractAsync };

export default connect(mapState, mapDispatch)(ContractManagementContainer);
