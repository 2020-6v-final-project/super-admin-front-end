import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CCol,
  CInputGroup,
  CRow
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { DataTable } from "@components";
import { ContractManagementDefaultProps } from "../../ContractManagement.container";
import { Contract } from '@custom-types';
import AddContractModal from "../AddContractModal/AddContractModal";
import ContractDetailModal from "../ContractDetailModal/ContractDetailModal";
import "./ListContracts.scss";

const ListContracts: React.FC<ContractManagementDefaultProps> = React.memo((props) => {

  const { fetchContractListAsync, addContractAsync, deactivateContractAsync, updateContractAsync } = props;
  //State for opening and closing modal
  const [openAddContractModal, setOpenAddContractModal] = useState<boolean>(
    false
  );
  const [openContractDetailModal, setOpenContractDetailModal] = useState<boolean>(
    false
  );

  const [contractDetail, setContractDetail] = useState<any>(null);

  useEffect(() => {
    fetchContractListAsync();
  }, [fetchContractListAsync]);


  const contractTableMapper = (item: any, index: number) => {
    return {
      ID: item.id,
      "Tên Công Ty": item.name,
      "Mã Công Ty": item.code,
      "Chủ Công Ty": item.owner,
      statuscode: "" || item.statuscode,
      numberofusers: 0 || item.numberofusers
    };
  }

  const contractTableField = [
    { key: "ID", _classes: "font-weight-bold" },
    "Tên Công Ty",
    "Ngày bắt đầu",
    "Ngày kết thúc",
    "Trạng Thái",
  ];

  const contractTableScopedSlots = {
    "Trạng Thái": (item: any) => (
      <td>
        <CBadge color={item.statuscode === 'AVAILABLE' ? "success" : "danger"}>{item.statuscode}</CBadge>
      </td>
    ),
  };

  const contractTableOnRowClick = (item: Contract) => { onContractDetailToggle(item); }

  const onAddContractToggle: any = () =>
    setOpenAddContractModal(!openAddContractModal);

  const onContractDetailToggle: any = (item: any) => {
    setContractDetail(item);
    setOpenContractDetailModal(!openContractDetailModal);
  };
  return (
    <CRow>
      <CCol>
        <AddContractModal
          isShow={openAddContractModal}
          onFinish={onAddContractToggle}
          addContractAsync={addContractAsync}
        />
        <ContractDetailModal
          deactivateContractAsync={deactivateContractAsync}
          updateContractAsync={updateContractAsync}
          isShow={openContractDetailModal}
          onClose={onContractDetailToggle}
          data={contractDetail}
        />
        <CCard>
          <CCardBody>
            <CInputGroup className="justify-content-between px-0 mx-0">
              <CRow className="col-md-12 px-0 my-2">
                <CCol sm="10">
                  <h4>Quản lý hợp đồng</h4>
                </CCol>
                <CCol className="px-0" sm="2">
                  <CButton
                    color="warning"
                    className="float-right"
                    style={{ color: "white" }}
                    onClick={onAddContractToggle}
                  >
                    {" "}
                    <CIcon
                      className="mr-2"
                      content={freeSet.cilLibraryBuilding}
                    />
                    Thêm hợp đồng
                  </CButton>
                </CCol>
              </CRow>
            </CInputGroup>
            <DataTable
              fields={contractTableField}
              items={props.contractList}
              itemsMapper={contractTableMapper}
              scopedSlots={contractTableScopedSlots}
              onRowClick={contractTableOnRowClick} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
});

export default ListContracts;
