import userPlusImg from "@assets/user-plus.png";
import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CCol, CForm,
  CInput,
  CInputGroup,
  CModal,
  CRow
} from "@coreui/react";
import { ContractError } from '@custom-types';
import { ContractSchema } from "@utils";
import React, { ChangeEvent, FormEvent, useState } from "react";
import swal from 'sweetalert';
import "./ContractDetailModal.scss";

type ContractDetailModalState = {
  isShow: boolean,
  data: any,
}

type ContractDetailModalDispatch = {
  deactivateContractAsync: Function,
  onClose: Function,
  updateContractAsync: Function,
}

type ContractDetailModalDefaultProps = ContractDetailModalState & ContractDetailModalDispatch;

const ContractDetailModal: React.FC<ContractDetailModalDefaultProps> = (props) => {

  const { isShow, deactivateContractAsync, onClose, data, updateContractAsync } = props;

  const [isEditMode, setEditMode] = useState(false);
  const toggleEditMode = () => {
    setEditMode(!isEditMode);
  };

  const [formData, setFormData] = useState({
    id: -1,
    companyid: -1,
    start: new Date(),
    end: new Date(),
    statusid: -1,
    statuscode: "",
    typeid: -1,
  });

  const [formDataError, setFormDataError] = useState<ContractError>({
    id: false,
    companyid: false,
    start: false,
    end: false,
    statusid: false,
    statuscode: false,
    typeid: false,
  });


  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };
  const handleSubmit =
    async (e: FormEvent) => {
      e.preventDefault();
      const contract = formData as any;
      contract.id = data.ID;
      contract.owner = parseInt(contract.owner);
      //Validaten form data
      ContractSchema.validate(formData, { abortEarly: false }).then(
        async (valid: Object) => {
          //Call server
          updateContractAsync(contract);
          setEditMode(false);
          onClose();
        },
        (errors: any) => {
          let newFormDataError: ContractError = { ...formDataError };

          for (let error of errors.errors)
          {
            let index: string = error.substring(0, error.indexOf(" "));
            newFormDataError[index] = true;
          }
          setFormDataError(newFormDataError);
          let keyDataError = "";
          for (const [key, value] of Object.entries(newFormDataError))
          {
            if (value === true) 
            {
              if (keyDataError !== "") keyDataError += ", ";
              keyDataError += key;
            }
          }
          if (keyDataError !== "")
            swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
        }
      );
    };

  const handleDeactivate = async () => {
    deactivateContractAsync(data.ID);
  }

  return (
    <CModal
      centered
      backdrop
      show={isShow}
      onClose={() => { setEditMode(false); onClose() }}
      className="ContractDetailModal-styles"
    >
      <CCard className="align-middle ContractDetailModal-card-styles">
        <CCardBody className="ContractDetailModal-cardBody-styles">
          <h4 className="ContractDetailModal-h2-styles">{data?.statuscode ? data["Tên Công Ty"] : "Thông tin công ty"}</h4>
          <img
            alt=""
            src={userPlusImg}
            className="ContractDetailModal-img-styles my-3"
          />
          <CRow className="my-3">
            <CCol className="ContractDetailModal-statistics-container">
              <h6 className="ContractDetailModal-light-text">Chủ công ty</h6>
              <h5>{data ? data["Chủ Công Ty"] : ""}</h5>
            </CCol>
            <CCol className="ContractDetailModal-statistics-container">
              <h6 className="ContractDetailModal-light-text">Nhân viên</h6>
              <h5>{data ? data.numberofusers : "0"}</h5>
            </CCol>
            <CCol className="ContractDetailModal-statistics-container">
              <h6 className="ContractDetailModal-light-text">Trạng thái</h6>
              <CBadge color={data?.statuscode === 'AVAILABLE' ? "success" : "danger"}>{data?.statuscode}</CBadge>
            </CCol>
          </CRow>

          <CRow className="my-3 justify-content-center">
            <CButton

              className="ContractDetailModal-optionButton-styles btn-outline-warning"
              onClick={toggleEditMode}
            >
              {isEditMode ? "Huỷ" : "Sửa thông tin"}
            </CButton>
            <CButton
              className="btn-outline"
              onClick={handleDeactivate}
            >
              Chặn người dùng
          </CButton>
          </CRow>

          <CForm className={isEditMode ? "ContractDetailModal-form" : "ContractDetailModal-form-hidden"} onSubmit={handleSubmit}>
            <CInputGroup style={{ marginTop: 20 }}>
              <CInput onChange={onChange}
                placeholder="Tên công ty"
                className="UserDetailModal-input-styles"
                type="text" name="name"
                defaultValue={data ? data["Tên Công Ty"] : ''} />
            </CInputGroup>
            <CInputGroup style={{ marginTop: 20 }}>
              <CRow>
                <CCol>
                  <CInput onChange={onChange}
                    placeholder="Mã công ty"
                    className="ContractDetailModal-input-styles"
                    type="text"
                    name="code"
                    defaultValue={data ? data["Mã Công Ty"] : ""}
                  />
                </CCol>

                <CCol>
                  <CInput onChange={onChange}
                    placeholder="Id chủ công ty"
                    className="ContractDetailModal-input-styles"
                    type="number"
                    name="owner"
                    defaultValue={data ? data["Chủ Công Ty"] : ""}
                  />
                </CCol>
              </CRow>
            </CInputGroup>

            <CButton
              color="primary"
              type="submit"
              className="ContractDetailModal-submitButton-styles my-3"
            >
              Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

export default ContractDetailModal;
