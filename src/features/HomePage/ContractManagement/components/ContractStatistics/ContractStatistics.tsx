import CIcon from '@coreui/icons-react';
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CFade,
    CRow,

    CWidgetProgressIcon
} from "@coreui/react";
import { CChartBar, CChartDoughnut } from "@coreui/react-chartjs";
import React from "react";
import "./ContractStatistics.scss";

const ContractStatistics: React.FC = (props) => {
    return (
        <CFade>
            <CRow>
                <CCol sm="6" lg="3">
                    <CWidgetProgressIcon
                        header="87.500"
                        text="Doanh thu tháng"
                        color="gradient-info"
                        inverse
                    >
                        <CIcon name="cil-dollar" height="36" />

                    </CWidgetProgressIcon>
                </CCol>
                <CCol sm="6" lg="3">
                    <CWidgetProgressIcon
                        header="385"
                        text="Tổng doanh thu"
                        color="gradient-success"
                        inverse
                    >
                        <CIcon name="cil-dollar" height="36" />
                    </CWidgetProgressIcon>
                </CCol>
                <CCol sm="6" lg="3">
                    <CWidgetProgressIcon
                        header="385"
                        text="Số công ty"
                        color="gradient-danger"
                        inverse
                    >
                        <CIcon name="cil-building" height="36" />
                    </CWidgetProgressIcon>
                </CCol>
                <CCol sm="6" lg="3">
                    <CWidgetProgressIcon
                        header="385"
                        text="Số hợp đồng"
                        color="gradient-warning"
                        inverse
                    >
                        <CIcon name="cil-clipboard" height="36" />
                    </CWidgetProgressIcon>
                </CCol>
            </CRow>

            <CRow>
                <CCol span={6}>
                    <CCard>
                        <CCardHeader>Bar Chart</CCardHeader>
                        <CCardBody>
                            <CChartBar
                                datasets={[
                                    {
                                        label: "GitHub Commits",
                                        backgroundColor: "#ff9b40",
                                        data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
                                    },
                                ]}
                                labels="months"
                                options={{
                                    tooltips: {
                                        enabled: true,
                                    },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol span={6}>
                    <CCard>
                        <CCardHeader>Doughnut Chart</CCardHeader>
                        <CCardBody>
                            <CChartDoughnut
                                datasets={[
                                    {
                                        backgroundColor: [
                                            "#41B883",
                                            "#E46651",
                                            "#00D8FF",
                                            "#DD1B16",
                                        ],
                                        data: [40, 20, 80, 10],
                                    },
                                ]}
                                labels={["VueJs", "EmberJs", "ReactJs", "AngularJs"]}
                                options={{
                                    tooltips: {
                                        enabled: true,
                                    },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </CFade>);
}

export default ContractStatistics;

