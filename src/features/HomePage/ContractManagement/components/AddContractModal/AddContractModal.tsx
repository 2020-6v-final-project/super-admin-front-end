import {
  CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,
  CInputGroup,
  CModal
} from "@coreui/react";
import React, { ChangeEvent, FormEvent, useState } from "react";
import swal from 'sweetalert';
import { Contract, ContractError } from '@custom-types';
import { ContractSchema } from "@utils";
import "./AddContractModal.scss";

type AddContractModalState = {
  isShow: boolean,
}

type AddContractModalDispatch = {
  addContractAsync: Function,
  onFinish: Function,
}

type AddContractModalDefaultProps = AddContractModalState & AddContractModalDispatch;

const AddContractModal: React.FC<AddContractModalDefaultProps> = (props) => {

  const { isShow, addContractAsync, onFinish } = props;

  const [formData, setFormData] = useState({
    id: -1,
    companyid: -1,
    start: new Date(),
    end: new Date(),
    statusid: -1,
    statuscode: "",
    typeid: -1,
  });

  const [formDataError, setFormDataError] = useState<ContractError>({
    id: false,
    companyid: false,
    start: false,
    end: false,
    statusid: false,
    statuscode: false,
    typeid: false,
  });

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const contract = formData as Contract;

    //Cast form data
    ContractSchema.cast(formData);

    //Validate form data
    ContractSchema.validate(formData, { abortEarly: false }).then(
      async (valid: Object) => {
        //Call server
        addContractAsync(contract);
        onFinish();
      },
      (errors: any) => {
        let newFormDataError: ContractError = { ...formDataError };

        for (let error of errors.errors)
        {
          let index: string = error.substring(0, error.indexOf(" "));
          newFormDataError[index] = true;
        }
        setFormDataError(newFormDataError);
        let keyDataError = "";
        for (const [key, value] of Object.entries(newFormDataError))
        {
          if (value === true) 
          {
            if (keyDataError !== "") keyDataError += ", ";
            keyDataError += key;
          }
        }
        if (keyDataError !== "")
          swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
      }
    );
  };

  return (
    <CModal
      centered
      backdrop
      width={200}
      closeOnBackdrop
      show={isShow}
      className="AddContractModal-styles"
    >
      <CCard className="align-middle justify-content-center jus AddContractModal-card-styles">
        <CCardBody className="AddContractModal-cardBody-styles">
          <h4 className="AddContractModal-h2-styles">Thêm công ty</h4>
          <img
            alt=""
            src="/user-plus.png"
            className="AddContractModal-img-styles mx-auto my-3"
          />
          <CForm onSubmit={handleSubmit}>
            <CInputGroup>
              <CInput
                onChange={onChange}
                placeholder="Tên công ty"
                className="AddContractModal-input-styles"
                type="text"
                name="name"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.contractname ? true : false}
                onChange={onChange}
                placeholder="Mã công ty"
                className="AddContractModal-input-styles"
                type="text"
                name="code"
              />
              <CInput
                invalid={formDataError.contractname ? true : false}
                onChange={onChange}
                placeholder="Id chủ công ty"
                className="AddContractModal-input-styles"
                type="text"
                name="owner"
              />
            </CInputGroup>

            <CButton
              type="submit"
              color="primary"
              className="AddContractModal-submitButton-styles btn my-3"
            >
              Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

export default AddContractModal;
