import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { Contract } from "@custom-types";
import {
  getContractList,
  addContract,
  updateContract,
  deactivateContract
} from "../../../shared/api/contractAPI";
import swal from "sweetalert";

// Types and state declaration
type ContractManagementState = {
  contractList: Contract[];
};

const initialState: ContractManagementState = {
  contractList: []
};

// Slice
const ContractManagementSlice = createSlice({
  name: "contractManagement",
  initialState,
  reducers: {
    addContractSuccess: (state, action: PayloadAction<Contract>) => {
      state.contractList.push(action.payload);
      return state;
    },
    addContractFailed: (state) => {
      return state;
    },
    deactivateContractSuccess: (state, action: PayloadAction<Contract>) => {
      let updatedContract: Contract = action.payload;
      const index = state.contractList.findIndex(
        (item: Contract) => item.id === updatedContract.id
      );

      state.contractList.splice(index, 1, updatedContract);
      return state;
    },
    updateContractSuccess: (state, action: PayloadAction<Contract>) => {
      let updatedContract: Contract = action.payload;
      const index = state.contractList.findIndex(
        (item: Contract) => item.id === updatedContract.id
      );

      state.contractList.splice(index, 1, updatedContract);
      return state;
    },
    updateContractFailed: (state) => {
      return state;
    },
    deactivateContractFailed: (state) => {
      return state;
    },
    fetchContractListSuccess: (state, action: PayloadAction<Contract[]>) => {
      state.contractList = action.payload;
      return state;
    },
    fetchContractListFailed: (state) => {
      return state;
    }
  }
});

// Thunk for async actions
export const fetchContractListAsync = (): AppThunk => async (dispatch) => {
  try {
    let contractList: Contract[];
    await getContractList((data: Contract[]) => {
      contractList = data;
      contractList.sort((item1: Contract, item2: Contract) => {
        return item1.id - item2.id;
      });
      dispatch(fetchContractListSuccess(contractList));
    });
  } catch (e) {
    dispatch(fetchContractListFailed());
  }
};

export const addContractAsync = (contract: Contract): AppThunk => async (
  dispatch
) => {
  try {
    let addedContract: Contract | null;
    await addContract(contract, (data: Contract) => {
      addedContract = data;
      swal("Thêm thành công!", "Công ty đã được thêm vào hệ thống!", "success");
      dispatch(addContractSuccess(addedContract));
    });
  } catch (e) {
    swal("Thêm không thành công!", "Vui lòng thử lại!", "error");
    dispatch(addContractFailed());
  }
};

export const deactivateContractAsync = (contractId: number): AppThunk => async (
  dispatch
) => {
  try {
    let updatedContract: Contract;
    await deactivateContract(contractId, (data: any) => {
      updatedContract = data.result as Contract;
      swal("Cập nhật thành công!", "Công ty đã được cập nhật!", "success");
      dispatch(deactivateContractSuccess(updatedContract));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", "Vui lòng thử lại!", "error");
    dispatch(deactivateContractFailed());
  }
};

export const updateContractAsync = (contract: Contract): AppThunk => async (
  dispatch
) => {
  try {
    let updatedContract: Contract;
    await updateContract(contract, (data: any) => {
      updatedContract = data.ContractInfo as Contract;
      swal("Cập nhật thành công!", "Công ty đã được cập nhật!", "success");
      dispatch(updateContractSuccess(updatedContract));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", "Vui lòng thử lại!", "error");
    dispatch(updateContractFailed());
  }
};

// Export part
export const {
  addContractSuccess,
  addContractFailed,
  deactivateContractSuccess,
  deactivateContractFailed,
  fetchContractListSuccess,
  fetchContractListFailed,
  updateContractSuccess,
  updateContractFailed
} = ContractManagementSlice.actions;

export default ContractManagementSlice.reducer;
