import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CCol,
  CInputGroup,
  CRow
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { DataTable } from "@components";
import { CompanyManagementDefaultProps } from "../../CompanyManagement.container";
import { Company } from '@custom-types';
import AddCompanyModal from "../AddCompanyModal/AddCompanyModal";
import CompanyDetailModal from "../CompanyDetailModal/CompanyDetailModal";
import "./ListCompanies.scss";

const ListCompanies: React.FC<CompanyManagementDefaultProps> = React.memo((props) => {

  const { fetchCompanyListAsync, addCompanyAsync, deactivateCompanyAsync, updateCompanyAsync } = props;
  //State for opening and closing modal
  const [openAddCompanyModal, setOpenAddCompanyModal] = useState<boolean>(
    false
  );
  const [openCompanyDetailModal, setOpenCompanyDetailModal] = useState<boolean>(
    false
  );

  const [companyDetail, setCompanyDetail] = useState<any>(null);

  useEffect(() => {
    console.log((React as any).icons)
    fetchCompanyListAsync();
  }, [fetchCompanyListAsync]);


  const companyTableMapper = (item: any, index: number) => {
    return {
      ID: item.id,
      "Tên Công Ty": item.name,
      "Mã Công Ty": item.code,
      "Chủ Công Ty": item.owner,
      statuscode: "" || item.statuscode,
      numberofusers: 0 || item.numberofusers
    };
  }

  const companyTableField = [
    { key: "ID", _classes: "font-weight-bold" },
    "Tên Công Ty",
    "Mã Công Ty",
    "Chủ Công Ty",
    "Trạng Thái",
  ];

  const companyTableScopedSlots = {
    "Trạng Thái": (item: any) => (
      <td>
        <CBadge color={item.statuscode === 'AVAILABLE' ? "success" : "danger"}>{item.statuscode}</CBadge>
      </td>
    ),
  };

  const companyTableOnRowClick = (item: Company) => { onCompanyDetailToggle(item); }

  const onAddCompanyToggle: any = () =>
    setOpenAddCompanyModal(!openAddCompanyModal);

  const onCompanyDetailToggle: any = (item: any) => {
    setCompanyDetail(item);
    setOpenCompanyDetailModal(!openCompanyDetailModal);
  };
  return (
    <CRow>
      <CCol>
        <AddCompanyModal
          isShow={openAddCompanyModal}
          onFinish={onAddCompanyToggle}
          addCompanyAsync={addCompanyAsync}
        />
        <CompanyDetailModal
          deactivateCompanyAsync={deactivateCompanyAsync}
          updateCompanyAsync={updateCompanyAsync}
          isShow={openCompanyDetailModal}
          onClose={onCompanyDetailToggle}
          data={companyDetail}
        />
        <CCard>
          <CCardBody>
            <CInputGroup className="justify-content-between px-0 mx-0">
              <CRow className="col-md-12 px-0 my-2">
                <CCol sm="10">
                  <h4>Quản lý công ty</h4>
                </CCol>
                <CCol className="px-0" sm="2">
                  <CButton
                    color="warning"
                    className="float-right"
                    style={{ color: "white" }}
                    onClick={onAddCompanyToggle}
                  >
                    {" "}
                    <CIcon
                      className="mr-2"
                      content={freeSet.cilLibraryBuilding}
                    />
                    Thêm công ty
                  </CButton>
                </CCol>
              </CRow>
            </CInputGroup>
            <DataTable
              fields={companyTableField}
              items={props.companyList}
              itemsMapper={companyTableMapper}
              scopedSlots={companyTableScopedSlots}
              onRowClick={companyTableOnRowClick} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
});

export default ListCompanies;
