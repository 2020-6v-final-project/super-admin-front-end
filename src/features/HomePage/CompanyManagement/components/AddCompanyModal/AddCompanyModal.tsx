import {
  CButton,
  CCard,
  CCardBody,
  CForm,
  CInput,
  CInputGroup,
  CModal
} from "@coreui/react";
import React, { ChangeEvent, FormEvent, useState } from "react";
import swal from 'sweetalert';
import { Company, CompanyError } from '@custom-types';
import { CompanySchema } from "@utils";
import "./AddCompanyModal.scss";

type AddCompanyModalState = {
  isShow: boolean,
}

type AddCompanyModalDispatch = {
  addCompanyAsync: Function,
  onFinish: Function,
}

type AddCompanyModalDefaultProps = AddCompanyModalState & AddCompanyModalDispatch;

const AddCompanyModal: React.FC<AddCompanyModalDefaultProps> = (props) => {

  const { isShow, addCompanyAsync, onFinish } = props;

  const [formData, setFormData] = useState({
    code: "",
    name: "",
    owner: null,
  });

  const [formDataError, setFormDataError] = useState<CompanyError>({
    code: false,
    name: false,
    owner: false,
  });

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const company = formData as Company;

    //Cast form data
    CompanySchema.cast(formData);

    //Validate form data
    CompanySchema.validate(formData, { abortEarly: false }).then(
      async (valid: Object) => {
        //Call server
        addCompanyAsync(company);
        onFinish();
      },
      (errors: any) => {
        let newFormDataError: CompanyError = { ...formDataError };

        for (let error of errors.errors)
        {
          let index: string = error.substring(0, error.indexOf(" "));
          newFormDataError[index] = true;
        }
        setFormDataError(newFormDataError);
        let keyDataError = "";
        for (const [key, value] of Object.entries(newFormDataError))
        {
          if (value === true) 
          {
            if (keyDataError !== "") keyDataError += ", ";
            keyDataError += key;
          }
        }
        if (keyDataError !== "")
          swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
      }
    );
  };

  return (
    <CModal
      centered
      backdrop
      width={200}
      closeOnBackdrop
      show={isShow}
      className="AddCompanyModal-styles"
    >
      <CCard className="align-middle justify-content-center jus AddCompanyModal-card-styles">
        <CCardBody className="AddCompanyModal-cardBody-styles">
          <h4 className="AddCompanyModal-h2-styles">Thêm công ty</h4>
          <img
            alt=""
            src="/user-plus.png"
            className="AddCompanyModal-img-styles mx-auto my-3"
          />
          <CForm onSubmit={handleSubmit}>
            <CInputGroup>
              <CInput
                onChange={onChange}
                placeholder="Tên công ty"
                className="AddCompanyModal-input-styles"
                type="text"
                name="name"
              />
            </CInputGroup>
            <CInputGroup>
              <CInput
                invalid={formDataError.companyname ? true : false}
                onChange={onChange}
                placeholder="Mã công ty"
                className="AddCompanyModal-input-styles"
                type="text"
                name="code"
              />
              <CInput
                invalid={formDataError.companyname ? true : false}
                onChange={onChange}
                placeholder="Id chủ công ty"
                className="AddCompanyModal-input-styles"
                type="text"
                name="owner"
              />
            </CInputGroup>

            <CButton
              type="submit"
              color="primary"
              className="AddCompanyModal-submitButton-styles btn my-3"
            >
              Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

export default AddCompanyModal;
