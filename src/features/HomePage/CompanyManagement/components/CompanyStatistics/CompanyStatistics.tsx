import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CFade,
    CRow,
    CWidgetSimple,
} from "@coreui/react";
import { CChartBar, CChartDoughnut } from "@coreui/react-chartjs";
import React from "react";
import "./CompanyStatistics.scss";
import { ChartBarSimple, ChartLineSimple } from '@components';

const CompanyStatistics: React.FC = (props) => {
    return (
        <CFade>
            <CRow>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartLineSimple
                            className="company-management-small-card"
                            borderColor="danger"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartLineSimple
                            className="company-management-small-card"
                            borderColor="primary"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartLineSimple
                            className="company-management-small-card"
                            borderColor="success"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartBarSimple
                            className="company-management-small-card"
                            backgroundColor="danger"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartBarSimple
                            className="company-management-small-card"
                            backgroundColor="primary"
                        />
                    </CWidgetSimple>
                </CCol>
                <CCol sm="4" lg="2">
                    <CWidgetSimple header="title" text="1,123">
                        <ChartBarSimple
                            className="company-management-small-card"
                            backgroundColor="success"
                        />
                    </CWidgetSimple>
                </CCol>
            </CRow>

            <CRow>
                <CCol span={6}>
                    <CCard>
                        <CCardHeader>Bar Chart</CCardHeader>
                        <CCardBody>
                            <CChartBar
                                datasets={[
                                    {
                                        label: "GitHub Commits",
                                        backgroundColor: "#ff9b40",
                                        data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
                                    },
                                ]}
                                labels="months"
                                options={{
                                    tooltips: {
                                        enabled: true,
                                    },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol span={6}>
                    <CCard>
                        <CCardHeader>Doughnut Chart</CCardHeader>
                        <CCardBody>
                            <CChartDoughnut
                                datasets={[
                                    {
                                        backgroundColor: [
                                            "#41B883",
                                            "#E46651",
                                            "#00D8FF",
                                            "#DD1B16",
                                        ],
                                        data: [40, 20, 80, 10],
                                    },
                                ]}
                                labels={["VueJs", "EmberJs", "ReactJs", "AngularJs"]}
                                options={{
                                    tooltips: {
                                        enabled: true,
                                    },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </CFade>);
}

export default CompanyStatistics;

