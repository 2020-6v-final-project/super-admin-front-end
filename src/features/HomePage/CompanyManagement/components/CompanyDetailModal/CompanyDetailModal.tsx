import userPlusImg from "@assets/user-plus.png";
import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CCol, CForm,
  CInput,
  CInputGroup,
  CModal,
  CRow
} from "@coreui/react";
import { CompanyError } from '@custom-types';
import { CompanySchema } from "@utils";
import React, { ChangeEvent, FormEvent, useState } from "react";
import swal from 'sweetalert';
import "./CompanyDetailModal.scss";

type CompanyDetailModalState = {
  isShow: boolean,
  data: any,
}

type CompanyDetailModalDispatch = {
  deactivateCompanyAsync: Function,
  onClose: Function,
  updateCompanyAsync: Function,
}

type CompanyDetailModalDefaultProps = CompanyDetailModalState & CompanyDetailModalDispatch;

const CompanyDetailModal: React.FC<CompanyDetailModalDefaultProps> = (props) => {

  const { isShow, deactivateCompanyAsync, onClose, data, updateCompanyAsync } = props;

  const [isEditMode, setEditMode] = useState(false);
  const toggleEditMode = () => {
    setEditMode(!isEditMode);
  };

  const [formData, setFormData] = useState({
    code: "",
    name: "",
    owner: "",
    id: ""
  });

  const [formDataError, setFormDataError] = useState<CompanyError>({
    code: false,
    name: false,
    owner: false,
  });


  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    setFormDataError({
      ...formDataError,
      [e.target.name]: false,
    });
  };
  const handleSubmit =
    async (e: FormEvent) => {
      e.preventDefault();
      const company = formData as any;
      company.id = data.ID;
      company.owner = parseInt(company.owner);
      //Validaten form data
      CompanySchema.validate(formData, { abortEarly: false }).then(
        async (valid: Object) => {
          //Call server
          updateCompanyAsync(company);
          setEditMode(false);
          onClose();
        },
        (errors: any) => {
          let newFormDataError: CompanyError = { ...formDataError };

          for (let error of errors.errors)
          {
            let index: string = error.substring(0, error.indexOf(" "));
            newFormDataError[index] = true;
          }
          setFormDataError(newFormDataError);
          let keyDataError = "";
          for (const [key, value] of Object.entries(newFormDataError))
          {
            if (value === true) 
            {
              if (keyDataError !== "") keyDataError += ", ";
              keyDataError += key;
            }
          }
          if (keyDataError !== "")
            swal("Lỗi", "Vui lòng điền thông tin " + keyDataError, "error");
        }
      );
    };

  const handleDeactivate = async () => {
    deactivateCompanyAsync(data.ID);
  }

  return (
    <CModal
      centered
      backdrop
      show={isShow}
      onClose={() => { setEditMode(false); onClose() }}
      className="CompanyDetailModal-styles"
    >
      <CCard className="align-middle CompanyDetailModal-card-styles">
        <CCardBody className="CompanyDetailModal-cardBody-styles">
          <h4 className="CompanyDetailModal-h2-styles">{data?.statuscode ? data["Tên Công Ty"] : "Thông tin công ty"}</h4>
          <img
            alt=""
            src={userPlusImg}
            className="CompanyDetailModal-img-styles my-3"
          />
          <CRow className="my-3">
            <CCol className="CompanyDetailModal-statistics-container">
              <h6 className="CompanyDetailModal-light-text">Chủ công ty</h6>
              <h5>{data ? data["Chủ Công Ty"] : ""}</h5>
            </CCol>
            <CCol className="CompanyDetailModal-statistics-container">
              <h6 className="CompanyDetailModal-light-text">Nhân viên</h6>
              <h5>{data ? data.numberofusers : "0"}</h5>
            </CCol>
            <CCol className="CompanyDetailModal-statistics-container">
              <h6 className="CompanyDetailModal-light-text">Trạng thái</h6>
              <CBadge color={data?.statuscode === 'AVAILABLE' ? "success" : "danger"}>{data?.statuscode}</CBadge>
            </CCol>
          </CRow>

          <CRow className="my-3 justify-content-center">
            <CButton

              className="CompanyDetailModal-optionButton-styles btn-outline-warning"
              onClick={toggleEditMode}
            >
              {isEditMode ? "Huỷ" : "Sửa thông tin"}
            </CButton>
            <CButton
              className="btn-outline"
              onClick={handleDeactivate}
            >
              Chặn người dùng
          </CButton>
          </CRow>

          <CForm className={isEditMode ? "CompanyDetailModal-form" : "CompanyDetailModal-form-hidden"} onSubmit={handleSubmit}>
            <CInputGroup style={{ marginTop: 20 }}>
              <CInput onChange={onChange}
                placeholder="Tên công ty"
                className="UserDetailModal-input-styles"
                type="text" name="name"
                defaultValue={data ? data["Tên Công Ty"] : ''} />
            </CInputGroup>
            <CInputGroup style={{ marginTop: 20 }}>
              <CRow>
                <CCol>
                  <CInput onChange={onChange}
                    placeholder="Mã công ty"
                    className="CompanyDetailModal-input-styles"
                    type="text"
                    name="code"
                    defaultValue={data ? data["Mã Công Ty"] : ""}
                  />
                </CCol>

                <CCol>
                  <CInput onChange={onChange}
                    placeholder="Id chủ công ty"
                    className="CompanyDetailModal-input-styles"
                    type="number"
                    name="owner"
                    defaultValue={data ? data["Chủ Công Ty"] : ""}
                  />
                </CCol>
              </CRow>
            </CInputGroup>

            <CButton
              color="primary"
              type="submit"
              className="CompanyDetailModal-submitButton-styles my-3"
            >
              Lưu
            </CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CModal>
  );
};

export default CompanyDetailModal;
