import { CFade } from "@coreui/react";
import React from "react";
import CompanyStatistics from './components/CompanyStatistics/CompanyStatistics';
import ListCompanies from "./components/ListCompanies/ListCompanies";
import { companyListSelector } from './CompanyManagement.selector';
import { connect } from 'react-redux';
import { Company } from '@custom-types';
import { fetchCompanyListAsync, addCompanyAsync, deactivateCompanyAsync, updateCompanyAsync } from './CompanyManagement.slice';
import { RootState } from 'src/store';

type CompanyManagementState = {
    companyList: Company[],
}

type CompanyManagementDispatch = {
    fetchCompanyListAsync: Function,
    addCompanyAsync: Function,
    deactivateCompanyAsync: Function,
    updateCompanyAsync: Function,
}

export type CompanyManagementDefaultProps = CompanyManagementState & CompanyManagementDispatch;

const CompanyManagementContainer: React.FC<CompanyManagementDefaultProps> = React.memo((props) => {
    return (
        <CFade>
            <CompanyStatistics />
            <ListCompanies {...props} />
        </CFade>
    );
});

const mapState = (state: RootState) => {
    return {
        companyList: companyListSelector(state),
    }
}
const mapDispatch = { fetchCompanyListAsync, addCompanyAsync, deactivateCompanyAsync, updateCompanyAsync };

export default connect(mapState, mapDispatch)(CompanyManagementContainer);
