import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "src/store";
import { Company } from "@custom-types";
import {
  getCompanyList,
  addCompany,
  updateCompany,
  deactivateCompany
} from "../../../shared/api/companyAPI";
import swal from "sweetalert";

// Types and state declaration
type CompanyManagementState = {
  companyList: Company[];
};

const initialState: CompanyManagementState = {
  companyList: []
};

// Slice
const CompanyManagementSlice = createSlice({
  name: "companyManagement",
  initialState,
  reducers: {
    addCompanySuccess: (state, action: PayloadAction<Company>) => {
      state.companyList.push(action.payload);
      return state;
    },
    addCompanyFailed: (state) => {
      return state;
    },
    deactivateCompanySuccess: (state, action: PayloadAction<Company>) => {
      let updatedCompany: Company = action.payload;
      const index = state.companyList.findIndex(
        (item: Company) => item.id === updatedCompany.id
      );

      state.companyList.splice(index, 1, updatedCompany);
      return state;
    },
    updateCompanySuccess: (state, action: PayloadAction<Company>) => {
      let updatedCompany: Company = action.payload;
      const index = state.companyList.findIndex(
        (item: Company) => item.id === updatedCompany.id
      );

      state.companyList.splice(index, 1, updatedCompany);
      return state;
    },
    updateCompanyFailed: (state) => {
      return state;
    },
    deactivateCompanyFailed: (state) => {
      return state;
    },
    fetchCompanyListSuccess: (state, action: PayloadAction<Company[]>) => {
      state.companyList = action.payload;
      return state;
    },
    fetchCompanyListFailed: (state) => {
      return state;
    }
  }
});

// Thunk for async actions
export const fetchCompanyListAsync = (): AppThunk => async (dispatch) => {
  try {
    let companyList: Company[];
    await getCompanyList((data: Company[]) => {
      companyList = data;
      companyList.sort((item1: Company, item2: Company) => {
        return item1.id - item2.id;
      });
      dispatch(fetchCompanyListSuccess(companyList));
    });
  } catch (e) {
    dispatch(fetchCompanyListFailed());
  }
};

export const addCompanyAsync = (company: Company): AppThunk => async (
  dispatch
) => {
  try {
    let addedCompany: Company | null;
    await addCompany(company, (data: Company) => {
      addedCompany = data;
      swal("Thêm thành công!", "Công ty đã được thêm vào hệ thống!", "success");
      dispatch(addCompanySuccess(addedCompany));
    });
  } catch (e) {
    swal("Thêm không thành công!", "Vui lòng thử lại!", "error");
    dispatch(addCompanyFailed());
  }
};

export const deactivateCompanyAsync = (companyId: number): AppThunk => async (
  dispatch
) => {
  try {
    let updatedCompany: Company;
    await deactivateCompany(companyId, (data: any) => {
      updatedCompany = data.result as Company;
      swal("Cập nhật thành công!", "Công ty đã được cập nhật!", "success");
      dispatch(deactivateCompanySuccess(updatedCompany));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", "Vui lòng thử lại!", "error");
    dispatch(deactivateCompanyFailed());
  }
};

export const updateCompanyAsync = (company: Company): AppThunk => async (
  dispatch
) => {
  try {
    let updatedCompany: Company;
    await updateCompany(company, (data: any) => {
      updatedCompany = data.CompanyInfo as Company;
      swal("Cập nhật thành công!", "Công ty đã được cập nhật!", "success");
      dispatch(updateCompanySuccess(updatedCompany));
    });
  } catch (e) {
    swal("Cập nhật không thành công!", "Vui lòng thử lại!", "error");
    dispatch(updateCompanyFailed());
  }
};

// Export part
export const {
  addCompanySuccess,
  addCompanyFailed,
  deactivateCompanySuccess,
  deactivateCompanyFailed,
  fetchCompanyListSuccess,
  fetchCompanyListFailed,
  updateCompanySuccess,
  updateCompanyFailed
} = CompanyManagementSlice.actions;

export default CompanyManagementSlice.reducer;
