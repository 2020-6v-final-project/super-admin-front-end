import { RootState } from "src/store";
import { createSelector } from "reselect";

export const companyListSelector = createSelector(
  [(state: RootState) => state.company.companyList],
  (companyList) => {
    return companyList;
  }
);
