import { freeSet } from '@coreui/icons';
import CIcon from '@coreui/icons-react';
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend, CInvalidFeedback, CLink,
  CRow
} from '@coreui/react';
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import load from '@assets/load.gif';
import loginpage_img from '@assets/loginpage_img.png';
import logo_trans from '@assets/logo_trans.png';
import { userActions } from '../../state/user/userActions';
import './LoginPage.scss';

const LoginPageContainer: React.FC = (props: any) => {
  const [Username, setUsername] = useState('');
  const [usernameInvalid, setUsernameInvalid] = useState(false);
  const [Password, setPassword] = useState('');
  const [passwordInvalid, setPasswordInvalid] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!localStorage.getItem('user'))
    {
      props.history.push('/login');
    }
  }, [props.history]);

  const handleUsernameChange = (e: any) => {
    if (e.target.value) setUsernameInvalid(false);
    setUsername(e.target.value);
  }

  const handlePasswordChange = (e: any) => {
    if (e.target.value) setPasswordInvalid(false);
    setPassword(e.target.value);
  }

  const handleSubmit = (e: any) => {

    e.preventDefault();
    e.stopPropagation();

    const { dispatch }: any = props;
    if (Username && Password)
    {
      setLoading(true);
      dispatch(userActions.login(Username, Password));
    }
    else
    {
      if (!Username) setUsernameInvalid(true);
      if (!Password) setPasswordInvalid(true);
      setLoading(false);
    }
  }
  const { loggingIn } = props;
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol className=" d-flex align-items-center justify-content-center " lg="12">
            <CCard className="align-middle loginCard-component shadow" >
              <CCardBody >
                <CRow className="justify-content-center" >
                  <CCol className=" d-flex align-items-center justify-content-center" sm="5">
                    <CForm className="text-center formLogin-component" name="form" onSubmit={handleSubmit} noValidate>
                      <img className="img-fluid logo-component" src={logo_trans} alt="logo_trans" />
                      <CInputGroup className={'form-group'}>
                        <CInputGroupPrepend >
                          <span className="input-group-text " id="username">
                            <CIcon content={freeSet.cilUser} />
                          </span>
                        </CInputGroupPrepend>
                        <CInput required placeholder="Tên tài khoản" className="input-login-component" invalid={usernameInvalid} type="text" autoComplete="username" name="username" value={Username} onChange={handleUsernameChange} />
                        <CInvalidFeedback>Vui lòng nhập tên đăng nhập</CInvalidFeedback>
                      </CInputGroup>

                      <CInputGroup className={'form-group'}>
                        <CInputGroupPrepend >
                          <span className="input-group-text" id="password">
                            <CIcon content={freeSet.cilUser} />
                          </span>
                        </CInputGroupPrepend>
                        <CInput required placeholder="Mật khẩu" className={"input-login-component"} invalid={passwordInvalid} type="password" autoComplete="current-password" name="password" value={Password} onChange={handlePasswordChange} />
                        <CInvalidFeedback>Vui lòng nhập tên mật khẩu</CInvalidFeedback>
                      </CInputGroup>

                      <CButton type="submit" className="loginButton-component btn-primary">
                        <span className="spinner-border spinner-border-sm mr-3" role="status" hidden={!loading} aria-hidden="true"></span>
                        Đăng nhập
                      </CButton>
                      {loggingIn && Password && Username &&
                        <img alt="load_img" src={load} />
                      }
                      <CLink href="">Quên mật khẩu</CLink>
                    </CForm>
                  </CCol>
                  <CCol>
                    <img className="img-fluid login_img_component" src={loginpage_img} alt="loginpage_img" />
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
}

function mapStateToProps(state: any) {
  const { loggingIn } = state;
  return { loggingIn };
}

export default connect(mapStateToProps)(LoginPageContainer);
