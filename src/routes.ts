import React from "react";
const Dashboard = React.lazy(
  () => import("./features/HomePage/dashboard/Dashboard")
);
const UserManagement = React.lazy(
  () => import("./features/HomePage/user-management/index")
);
const CompanyManagement = React.lazy(
  () => import("./features/HomePage/CompanyManagement/index")
);
const ContractManagement = React.lazy(
  () => import("@features/HomePage/ContractManagement")
);

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/user-management",
    exact: true,
    name: "User Management",
    component: UserManagement
  },
  {
    path: "/company-management",
    exact: true,
    name: "Company Management",
    component: CompanyManagement
  },
  {
    path: "/contract-management",
    exact: true,
    name: "Contract Management",
    component: ContractManagement
  }
];

export default routes;
