import { userTypes } from "./userTypes";
import { userAPI } from "../../shared/api/userAPI";

export const userActions = {
  login,
  logout,
  getUserList,
  setUserList,
};

function login(username: string, password: string) {
  return (dispatch: any) => {
    dispatch(request({ username }));

    userAPI.login(username, password).then(
      (user) => {
        dispatch(success(user));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request(user: any) {
    return { type: userTypes.LOGIN_REQUEST, user };
  }
  function success(user: any) {
    return { type: userTypes.LOGIN_SUCCESS, user };
  }
  function failure(error: any) {
    return { type: userTypes.LOGIN_FAILURE, error };
  }
}

function logout() {
  userAPI.logout();
  return { type: userTypes.LOGOUT };
}

export function getUserList() {
  return { type: userTypes.GET_USER_LIST };
}

export function setUserList(payload: any) {
  return { type: userTypes.SET_USER_LIST, payload: payload };
}
