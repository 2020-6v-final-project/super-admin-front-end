import { userTypes } from "./userTypes";

const initialState = {
  loading: false,
  items: [],
  error: {},
  userList: [],
};

export function userReducer(state = initialState, action: any) {
  switch (action.type) {
    case userTypes.GETALL_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case userTypes.GETALL_SUCCESS:
      return {
        ...state,
        items: action.users,
      };
    case userTypes.GETALL_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    case userTypes.SET_USER_LIST: {
      return {
        ...state,
        userList: action.payload,
      };
    }
    default:
      return state;
  }
}
