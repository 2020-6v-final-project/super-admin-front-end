import { userTypes } from '../user/userTypes';

let user = localStorage.getItem('user');
const initialState = user ? { loggedIn: true, user } : {};

export function authReducer(state = initialState, action: any) {
  switch (action.type)
  {
    case userTypes.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userTypes.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userTypes.LOGIN_FAILURE:
      return {};
    case userTypes.LOGOUT:
      return {};
    default:
      return state
  }
}